# Install script for directory: /home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OculusSDK/Include" TYPE FILE FILES
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Include/OVR.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Include/OVRVersion.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OculusSDK/Src" TYPE FILE FILES
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_SensorFilter.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Win32_DeviceManager.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Win32_HIDDevice.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_OSX_HMDDevice.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Win32_HMDDevice.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_DeviceConstants.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Profile.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_OSX_HIDDevice.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_OSX_DeviceManager.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_DeviceHandle.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_HIDDevice.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Win32_DeviceStatus.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_HIDDeviceBase.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_JSON.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_DeviceImpl.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_HIDDeviceImpl.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Win32_SensorDevice.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Device.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_SensorFusion.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_ThreadCommandQueue.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_LatencyTestImpl.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_SensorImpl.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_DeviceMessages.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Linux_HIDDevice.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Linux_DeviceManager.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/OVR_Linux_HMDDevice.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OculusSDK/Src/Kernel" TYPE FILE FILES
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Threads.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Types.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_System.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_RefCount.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Array.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_StringHash.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Color.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Hash.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Std.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_File.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Atomic.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_String.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Math.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_UTF8Util.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_ContainerAllocator.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_KeyCodes.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Timer.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Allocator.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_List.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Alg.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_SysFile.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Kernel/OVR_Log.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/OculusSDK/Src/Util" TYPE FILE FILES
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Util/Util_Render_Stereo.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Util/Util_MagCalibration.h"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/Src/Util/Util_LatencyTest.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libovr.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libovr.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libovr.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/libovr.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libovr.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libovr.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libovr.so")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "udev")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/etc/udev/rules.d/90-oculus-hydro.rules")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/etc/udev/rules.d" TYPE FILE FILES "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/90-oculus-hydro.rules")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/OculusSDK" TYPE FILE FILES
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR//CMakeFiles/OculusSDKConfig.cmake"
    "/home/tman/ros_catkin_ws/src/ros-oculus-monocular_viewer/LibOVR/LibOVR/OculusSDKConfigVersion.cmake"
    )
endif()

